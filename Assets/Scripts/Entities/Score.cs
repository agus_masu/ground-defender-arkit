﻿namespace Entities
{
    public class Score
    {
        public string name;
        public int level;

        public Score(string name, int level)
        {
            this.name = name;
            this.level = level;
        }
    }
}