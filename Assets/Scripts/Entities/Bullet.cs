﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

	public float damage;
	public string treasureTag = "Treasure";
	public string skeletonTag = "Skeleton";
	float lifetime = 2.0f;
	public float speed = 2;
	
	void Awake()
	{
		//Destroys the bullet after a certain time
		Destroy(gameObject, lifetime);
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += Time.deltaTime * speed * transform.forward;
	}
	

	private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.CompareTag(treasureTag))
		{
			other.gameObject.GetComponent<Treasure>().PermormDamage(damage);
			Debug.Log("Damage Performed");
			Destroy(gameObject);
		}
		else if (other.gameObject.CompareTag(skeletonTag))
		{
			other.gameObject.GetComponent<Skeleton>().HitSkeleton();
			Destroy(gameObject);
		}

	}
}
