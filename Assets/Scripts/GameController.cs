﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	
	public static GameController instance = null;
	private GameState gameState;
	private GameState flowerPlacing, shootingState;
	public float distance = 100;
	public string gameObjectBoardTag = "Board";
	public float cameraDistance = 0f;
	private int remainingBullets;
	private int actualLevel = 0;
	public TextMeshProUGUI statusText, finalLevelText;
	public GameObject shootingUIElements, finalUIElements;
	public LayerMask collisionLayer;  //ARKitPlane layer
	public GameObject board;
	private bool hasStarted = false;
	private bool hasFinisshedSpawning = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		TestSkeleton();
		TestTrasures();
		if (Input.GetMouseButtonDown(0))
		{
			PerformTouchAction(Input.mousePosition);
			
		}
		else if (Input.touchCount > 0)
		{
			PerformTouchAction(Input.GetTouch(0).position);
		}
		else if (Input.GetKeyDown(KeyCode.S))
		{
			Shoot();
		}

	}
	
	void Awake()
	{
		//Check if instance already exists
		if (instance == null)
                
			//if not, set instance to this
			instance = this;
            
		//If instance already exists and it's not this:
		else if (instance != this)
                
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);    
            
		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);
           
		//Call the InitGame function to initialize the first level 
		InitGame();
	}

	private void InitGame()
	{
		finalUIElements.SetActive(false);
		shootingState = new ShootingState();
		flowerPlacing = new FlowerPlacingState();

		SwitchToTrasuresPlacing();

	}

	public void GenerateTreasure(Vector3 position)
	{
		GameObject treasure = Instantiate(Resources.Load("Prefabs/Treasure", typeof(GameObject))) as GameObject;
		System.Diagnostics.Debug.Assert(treasure != null, "treasure != null");
		treasure.transform.position = position;

	}

	public void PerformShooting()
	{
		Camera mainCamera = Camera.main;
		
		GameObject bullet = Instantiate(Resources.Load("Prefabs/Bullet", typeof(GameObject))) as GameObject;
		//GameObject bullet = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		bullet.transform.position = mainCamera.transform.position +mainCamera.transform.forward*cameraDistance;
		bullet.transform.rotation = mainCamera.transform.rotation;
		SoundController.instance.PlaySound("shooting", mainCamera.transform.position);
		

		//bullet.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
		//Instantiate(creation, transform.position + transform.forward*distance, transform.rotation);

	}

	public void SwitchToShooting()
	{
		statusText.text = "Destruye los Esqueletos";
		actualLevel++;
		StartCoroutine(SetSkeletonsInTime(actualLevel));
		remainingBullets = 30;
		
		Debug.Log("Changed to Shooting");
		gameState = shootingState;
		gameState.OnInit();
		shootingUIElements.SetActive(true);
	}
	
	public void SwitchToTrasuresPlacing()
	{
		statusText.text = "Posiciona Sus Tesoros";
		remainingBullets = 0;
		Debug.Log("Changed to Treasure Placing");
		gameState = flowerPlacing;
		gameState.OnInit();
		shootingUIElements.SetActive(false);
	}

	public void Shoot()
	{
		if (remainingBullets > 0)
		{
			gameState.OnShootPressed();
			remainingBullets--;
		}
	}

	private void PerformTouchAction(Vector3 touchPosition)
	{
		if (EventSystem.current.IsPointerOverGameObject(0)) return;
		Ray ray = Camera.main.ScreenPointToRay (touchPosition);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, distance, collisionLayer)) 
		{
			//draw invisible ray cast/vector
			//Debug.DrawLine (ray.origin, hit.point);
			//log hit area to the console
			//Debug.Log(hit.point);
				
			gameState.OnTouchPressed(gameObjectBoardTag, hit);
                                   
		}    
	}

	public int GetRemainingBullets()
	{
		return remainingBullets;
	}

	private IEnumerator SetSkeletonsInTime(int skeletonNumber)
	{
		hasFinisshedSpawning = false;
		for (int i = 0; i < skeletonNumber; i++)
		{
			GameObject skeleton = Instantiate(Resources.Load("Prefabs/Skeleton", typeof(GameObject))) as GameObject;
			Vector3 position = board.transform.position;
			skeleton.transform.position = new Vector3(position.x, position.y + 0.5f, position.z);
			//skeleton.transform.position = new Vector3(0, -0.5f, 2);
			yield return new WaitForSeconds(1);
		}

		hasFinisshedSpawning = true;
	}

	private void TestSkeleton()
	{
		if (!hasFinisshedSpawning) return;
		if (gameState.GetStateName().Equals("Shooting") && GameObject.FindGameObjectsWithTag(Skeleton.tag).Length == 0)
		{
			foreach (GameObject treasure in GameObject.FindGameObjectsWithTag("Treasure"))
			{
				Destroy(treasure);
			}
			SwitchToTrasuresPlacing();
		}
	}

	private void TestTrasures()
	{
		if (gameState.GetStateName().Equals("Shooting") && GameObject.FindGameObjectsWithTag("Treasure").Length == 0)
		{
			foreach (GameObject skeleton in GameObject.FindGameObjectsWithTag(Skeleton.tag))
			{
				Destroy(skeleton);
			}

			statusText.text = "Perdiste";
			shootingUIElements.SetActive(false);
			finalUIElements.SetActive(true);
			finalLevelText.text = actualLevel.ToString();

		}
	}

	public int GetActualLevel()
	{
		return actualLevel;
	}

	public void StartPlacingTreasures()
	{
		hasStarted = true;
	}


}
