﻿using UnityEngine;

namespace DefaultNamespace
{
    public interface GameState
    {
        void OnTouchPressed(string boardTag, RaycastHit hit);

        void OnShootPressed();

        void OnInit();

        string GetStateName();
    }
}