﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using Entities;
using TMPro;
using UnityEngine;

public class ScoreSaver : MonoBehaviour
{
	public TMP_InputField field;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SaveScore()
	{
		string name = field.text;
		int level = GameController.instance.GetActualLevel();
		
		Score score = new Score(name, level);
		
		ScoreRecorder.AddScore(score);
	}
}
