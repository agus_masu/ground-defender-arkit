﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAnimator : MonoBehaviour
{
	private Animator _animator;
	

	// Use this for initialization
	void Start () {
		
	}

	void Awake()
	{
		_animator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void SetSpeed(float speed)
	{
		_animator.SetFloat("speed", speed);
	}

	public void SetAttack(bool attack)
	{
		_animator.SetBool("attack", attack);
	}
	
	
}
