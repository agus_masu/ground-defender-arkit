﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : MonoBehaviour
{

	public static float startingHealth = 100;
	private float _health;
	
	// Use this for initialization
	void Start () {
		
		
	}

	void Awake()
	{
		_health = startingHealth;
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void PermormDamage(float damage)
	{
		if (_health - damage <= 0)
		{
			Debug.Log("Treasure Destroyed");
			Destroy(gameObject);
		} else
		{
			_health -= damage;
		}
	}
	
	
}
