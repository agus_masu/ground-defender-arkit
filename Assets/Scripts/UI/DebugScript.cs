﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DebugScript : MonoBehaviour
{

	public GameObject debugUI;
	public TextMeshProUGUI debugText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		GameObject[] planes = GameObject.FindGameObjectsWithTag("Board");
		debugText.text = "DEBUG: \nBoard Planes: " + planes.Length.ToString()+ "\n \n";
		int colliders = planes.Count(plane => plane.GetComponent<MeshCollider>() != null);
		debugText.text += "Planes With Colliders: " + colliders;
	}

	void ToggleUI()
	{
		debugUI.SetActive(!debugUI.activeSelf);
	}

}
