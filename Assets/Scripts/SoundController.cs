﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{

	private AudioSource _audioSource;
	public AudioClip treasureDestroyed, shooting, treasurePunch, skeletonDestroyed;
	public static SoundController instance = null;
	

	
	private void Awake()
	{
		//Check if instance already exists
		if (instance == null)
                
			//if not, set instance to this
			instance = this;
            
		//If instance already exists and it's not this:
		else if (instance != this)
                
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);    
            
		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);
		
		_audioSource = GetComponent<AudioSource>();

           
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public 	void PlaySound(string type, Vector3 position)
	{
		switch (type)
		{
				case "treasurePunch":
					AudioSource.PlayClipAtPoint(treasurePunch, position);
					break;
				case "skeletonDestroyed":
					AudioSource.PlayClipAtPoint(skeletonDestroyed, position);
					break;
				case "shooting":
					AudioSource.PlayClipAtPoint(shooting, position);
					break;
		}
	}
}
