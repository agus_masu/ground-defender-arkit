﻿using UnityEngine;

namespace DefaultNamespace
{
    public class FlowerPlacingState : GameState
    {

        private readonly int treasuresToPlace = 4;
        private int remainingTreasures = 0;
        private enum MCFace
        {
            None,
            Up,
            Down,
            East,
            West,
            North,
            South
        }
        
        public void OnTouchPressed(string boardTag, RaycastHit hit)
        {
            if (
                hit.transform.gameObject.CompareTag(boardTag) && 
                GetHitFace(hit) == MCFace.Up && 
                remainingTreasures > 0)
            {
                Vector3 position = hit.point;
                GameController.instance.GenerateTreasure(position);
                remainingTreasures--;
            }
            else Debug.Log("Tags are Not the Same or Not the Upper Face");
            
            if (remainingTreasures == 0) GameController.instance.SwitchToShooting();
        }
 
        private MCFace GetHitFace(RaycastHit hit)
        {
            Vector3 incomingVec = hit.normal - Vector3.up;
 
            if (incomingVec == new Vector3(0, -1, -1))
                return MCFace.South;
 
            if (incomingVec == new Vector3(0, -1, 1))
                return MCFace.North;

            if (incomingVec == new Vector3(0, 0, 0))
                return MCFace.Up;
 
            if (incomingVec == new Vector3(1, 1, 1))
                return MCFace.Down;
 
            if (incomingVec == new Vector3(-1, -1, 0))
                return MCFace.West;
 
            if (incomingVec == new Vector3(1, -1, 0))
                return MCFace.East;
 
            return MCFace.None;
        }

        public void OnShootPressed()
        {
            Debug.Log("CantShoot When Placing Treasures");
        }


        public void OnInit()
        {
            remainingTreasures = treasuresToPlace;
        }

        public string GetStateName()
        {
            return "Placing";
        }
    }
}