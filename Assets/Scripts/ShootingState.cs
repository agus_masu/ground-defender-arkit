﻿using UnityEngine;

namespace DefaultNamespace
{
    public class ShootingState : GameState
    {
        public void OnTouchPressed(string boardTag, RaycastHit hit)
        {
            Debug.Log("Cant Place Treasure");
        }

        public void OnShootPressed()
        {
            GameController.instance.PerformShooting();
        }

        public void OnInit()
        {
            
        }

        public string GetStateName()
        {
            return "Shooting";
        }
    }
}