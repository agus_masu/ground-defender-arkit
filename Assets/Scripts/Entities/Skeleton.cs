﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using UnityEngine;

public class Skeleton : MonoBehaviour
{
	public static string treasuresTag = "Treasure";
	private GameObject _target;
	public float speed = 3;
	private bool _walk;
	private SkeletonAnimator _animator;
	public float damage = 20;
	public int hitLeft = 3;
	private bool _isCoroutineRunning = false;
	public static string tag = "Skeleton";

	// Use this for initialization
	void Start ()
	{
		_animator = GetComponent<SkeletonAnimator>();
		FindNewTarget();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (_target == null)
		{
			FindNewTarget();
		}	
		if (_target == null) return;
		Debug.Log("Target Not Null");
		LookAtTreasure();
		if (_walk) transform.position += transform.forward * speed * 0.001f;
	}

	private void FindNewTarget()	
	{
		_animator.SetAttack(false);
		//_target = GameObject.FindGameObjectWithTag(treasuresTag);
		GameObject[] target = GameObject.FindGameObjectsWithTag(treasuresTag);
		_target = FindClosest(target);
		if (_target == null) return;
		SetSpeed(3);
	}

	private GameObject FindClosest(GameObject[] gameObjects)
	{
		if (gameObjects.Length == 0) return null;
		GameObject actual = gameObjects[0];
		float distance = Vector3.Distance(actual.transform.position, transform.position);
		foreach (GameObject go in gameObjects)
		{
			if (Vector3.Distance(transform.position, go.transform.position) < distance)
			{
				actual = go;
				distance = Vector3.Distance(transform.position, go.transform.position);
			}
		}
		return actual;
	}

	private void LookAtTreasure()
	{
		transform.LookAt(_target.transform);
		Quaternion actualRotation = transform.rotation;
		transform.Rotate(new Vector3(0, -actualRotation.y, 0));

	}

	void OnCollisionEnter(Collision other)
	{
		if (other.collider.CompareTag("Treasure"))
		{
			_target = other.gameObject;
			SetSpeed(0);
			_animator.SetAttack(true);
			StartCoroutine(AttackingCoroutine());
		}
	}
	
	private void SetSpeed(float speed)
	{
		this.speed = speed;
		if (speed == 0) _walk = false;
		else _walk = true;
		_animator.SetSpeed(speed);
		
	}

	public void HitSkeleton()
	{
		if ((hitLeft - 1) <= 0)
		{
			SoundController.instance.PlaySound("skeletonDestroyed", transform.position);
			Destroy(gameObject);
		}
		else hitLeft--;
	}

	private IEnumerator AttackingCoroutine()
	{	
		Treasure treasure = _target.GetComponent<Treasure>();
		while (_target != null)
		{
			SoundController.instance.PlaySound("treasurePunch", transform.position);
			treasure.PermormDamage(damage);
			yield return new WaitForSeconds(1);
		}

	}
}
