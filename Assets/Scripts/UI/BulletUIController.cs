﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BulletUIController : MonoBehaviour
{
	private GameController controller;
	private TextMeshProUGUI _textToModify;
	
	// Use this for initialization
	void Start ()
	{
		_textToModify = GetComponent<TextMeshProUGUI>();
		controller = GameController.instance;
	}
	
	// Update is called once per frame
	void Update ()
	{
		_textToModify.text = controller.GetRemainingBullets().ToString();
	}
}
